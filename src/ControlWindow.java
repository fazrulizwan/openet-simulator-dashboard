import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Frame;
import javax.swing.JLabel;
import javax.swing.JSplitPane;


public class ControlWindow extends JFrame {

	private JPanel contentPane;
	Context ctx;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//ControlWindow frame = new ControlWindow();
					//frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ControlWindow(final Context ctx) {
		
		final JLabel lblSimName = new JLabel("");
		final JButton btnStop = new JButton("");
		final JLabel lblSimRun = new JLabel("");
		
		this.ctx = ctx;
		setTitle("Simulator Control");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 176);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		contentPane.add(toolBar, BorderLayout.CENTER);
		
		final JButton btnPlay = new JButton("");
		btnPlay.setEnabled(false);
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ctx.getMode() == 1) {
					if(ctx.getRemoteConnector().run() == 0) {					
						btnPlay.setEnabled(false);
						btnStop.setEnabled(true);
						lblSimRun.setText("Simulator is running");
					}
				} else if(ctx.getMode() == 2) {
					ctx.getCSVReader().run();
				}
			}
		});
		btnPlay.setIcon(new ImageIcon("C:\\Users\\fazruli\\Documents\\JIRA-WS\\jchart_project\\images\\play_button.gif"));
		toolBar.add(btnPlay);
		
				
		btnStop.setEnabled(false);
		btnStop.setIcon(new ImageIcon("C:\\Users\\fazruli\\Documents\\JIRA-WS\\jchart_project\\images\\StopIcon.jpg"));
		toolBar.add(btnStop);		
		
		
		toolBar.add(lblSimRun);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.NORTH);
		
		final JLabel label = new JLabel("");
		label.setOpaque(true);
		splitPane.setLeftComponent(label);
		
		final JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ctx.getMode() == 1) {
					int estStatus = ctx.getRemoteConnector().establish();
					if(estStatus == 0) {
						ctx.setSimServConnected(true);
						label.setText("Connected to simulator");
						label.setBackground(Color.GREEN);
						btnConnect.setEnabled(false);
						lblSimName.setText("Simulator: [" + ctx.getSimulatorName() + "] [Ver: "+ctx.getSimulatorVersion()+"]");
						lblSimRun.setText("simulator is not running");
						btnPlay.setEnabled(true);
						ctx.getWindowManager().showChartWindows();
					}
				} else if(ctx.getMode() == 2) {
					label.setBackground(Color.RED);
					label.setText("Loading playback data...");
					if(ctx.getCSVReader().establish() == 0) {
						label.setText("Playback mode ready");
						label.setBackground(Color.GREEN);
						btnConnect.setEnabled(false);
						//lblSimName.setText("Simulator: [" + ctx.getSimulatorName() + "] [Ver: "+ctx.getSimulatorVersion()+"]");
						lblSimRun.setText("playback stopped");
						btnPlay.setEnabled(true);
						ctx.getWindowManager().showChartWindows();
					}
				}
				
			}
		});
		
		
		splitPane.setRightComponent(btnConnect);
		
		
		contentPane.add(lblSimName, BorderLayout.SOUTH);
		if(ctx.isSimServConnected()) {
			label.setText("Connected to simulator");
			label.setBackground(Color.GREEN);
		}
		else {
			label.setText("Not connected to simulator");
			label.setBackground(Color.RED);			
		}
		
	}	

}
