import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;


public class ConnToSimWindow {

	private JFrame frmConnectToSimulator;
	private JTextField txtHost;
	private JTextField textField;

	
	/**
	 * Create the application.
	 */
	public ConnToSimWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConnectToSimulator = new JFrame();
		frmConnectToSimulator.setTitle("Connect to simulator");
		frmConnectToSimulator.setBounds(100, 100, 450, 189);
		frmConnectToSimulator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmConnectToSimulator.getContentPane().setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(21, 24, 46, 14);
		frmConnectToSimulator.getContentPane().add(lblHost);
		
		txtHost = new JTextField();
		txtHost.setBounds(58, 21, 86, 20);
		frmConnectToSimulator.getContentPane().add(txtHost);
		txtHost.setColumns(10);
		
		JLabel lblPort = new JLabel("port:");
		lblPort.setBounds(21, 49, 31, 14);
		frmConnectToSimulator.getContentPane().add(lblPort);
		
		textField = new JTextField();
		textField.setBounds(58, 46, 86, 20);
		frmConnectToSimulator.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.setBounds(165, 99, 89, 23);
		frmConnectToSimulator.getContentPane().add(btnOk);
	}
}
