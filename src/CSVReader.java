import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class CSVReader implements Runnable {
	
	private int interval = 1000;
	private int accel = 1;
	private boolean running = true;
	Context ctx;
	String dataFile;
	
	public CSVReader(Context ctx, String dataFile) {
		this.ctx = ctx;
		this.dataFile = dataFile;
	}
	
	public void setAccelRate(int val) {
		accel = val;
	}
	
	private int getDataCount() {
		
		FileInputStream file = null;
		try {
			file = new FileInputStream(dataFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		InputStreamReader inReader = new InputStreamReader(file);
		BufferedReader reader = null;
		String line;
		reader = new BufferedReader(inReader);
		int counter = 1;
		
		try {
			while((line = reader.readLine()) != null) {				
				counter++;
			}
			return counter;
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
		
	}
	
		
	public void run() {
		ctx.getPlayBack().setProgressMax(getDataCount());
		FileInputStream file = null;
		try {
			file = new FileInputStream(dataFile);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		InputStreamReader inReader = new InputStreamReader(file);
		BufferedReader reader = null;
		String line;
		reader = new BufferedReader(inReader);
		int counter = 1;
		int fieldCount = 0;
		String[] fieldId = {};
		JSONObject jsonObj;
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			while((line = reader.readLine()) != null) {
				
				try {
					Thread.sleep(interval/accel);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println(line);
				
				if(counter == 1) {
					fieldId = line.split(",");
					fieldCount = fieldId.length;
				}
				else {
					
					
					Map respMap2 = new LinkedHashMap();				
	            	respMap2.put("stats_data_count", String.valueOf(fieldCount));
	            	
	            	for(int i=0; i<fieldCount; i++) {
	            		String[] value = line.split(",");
	            		respMap2.put(fieldId[i], value[i]);
	            	}           	
	            	
	            	String dataString;
	            	dataString = JSONValue.toJSONString(respMap2);
	            	try {
						obj = parser.parse(dataString);
						jsonObj = (JSONObject) obj;
						if(ctx.getDataProcessor().processData(jsonObj) == 0) {
	                		for(int i=0; i<ctx.getWindowManager().getChartWin().size(); i++) {
	                			ChartWin win = ctx.getWindowManager().getChartWin().get(i);
	                			win.updateChart(ctx.getDataProcessor());
	                		}	                		
	                	}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}           
				}
            	ctx.getPlayBack().setProgress(counter);
				
				counter++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
