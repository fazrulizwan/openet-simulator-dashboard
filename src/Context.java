
public class Context {

	private boolean isSimServConnected = false;
	private boolean isSimRunning = false;
	private RemoteConnector remoteConn;
	private ChartWindowManager winMan;
	private String simName;
	private String simVer;
	private StatsDataProcessor dataProc;
	private CSVReaderManager csvReader;
	private int mode;
	private PlayBackWin pb;
	MDI mdi;
	private String simHost;
	private int simPort;
	
	public void setSimServConnected(boolean val) { isSimServConnected = val; }
	public void setSimRunning(boolean val) { isSimRunning = val; }
	public void setRemoteConnector(RemoteConnector remoteConn) { this.remoteConn = remoteConn; }
	public void setWindowManager(ChartWindowManager winMan) { this.winMan = winMan; }
	public void setSimulatorName(String val) { simName = val; }
	public void setSimulatorVersion(String val) { simVer = val; }
	public void setDataProcessor(StatsDataProcessor val) { dataProc = val; }
	public void setCSVReader(CSVReaderManager val) { csvReader = val; }
	public void setMode(int val) { mode = val; }
	public void setPlayBack(PlayBackWin val) { pb = val; }
	public void setMDI(MDI val) { mdi = val; }
	public void setSimHost(String val) { simHost = val; }
	public void setSimPort(int val) { simPort = val; }
	
	public boolean isSimServConnected() { return isSimServConnected; }
	public boolean isSimRunning() { return isSimRunning; }
	public RemoteConnector getRemoteConnector() { return remoteConn; }
	public ChartWindowManager getWindowManager() { return winMan; }
	public String getSimulatorName() { return simName; }
	public String getSimulatorVersion() { return simVer; }
	public StatsDataProcessor getDataProcessor() { return dataProc; }
	public CSVReaderManager getCSVReader() { return csvReader; }
	public int getMode() { return mode; }
	public PlayBackWin getPlayBack() { return pb; }	
	public MDI getMDI() { return mdi; }
	public String getSimHost() { return simHost; }
	public int getSimPort() { return simPort; }
}
