import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JProgressBar;


public class Playback {

	private JFrame frmPlayback;
	Context ctx;
	JLabel lblAccel = new JLabel("1X");
	JProgressBar progressBar = new JProgressBar();

	/**
	 * Create the application.
	 */
	public Playback(Context ctx) {
		this.ctx = ctx;
		initialize();
	}

	public void setProgress(int val) {
		progressBar.setValue(val);		
	}
	
	public void setProgressMax(int val) {
		progressBar.setMaximum(val);		
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPlayback = new JFrame();
		frmPlayback.setTitle("Playback");
		frmPlayback.setBounds(100, 100, 450, 127);
		frmPlayback.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPlayback.getContentPane().setLayout(null);
		
		JButton btn1X = new JButton("1X");
		btn1X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ctx.getCSVReader().setAccelRate(1);
				lblAccel.setText("1X");
			}
		});
		btn1X.setBounds(10, 0, 57, 23);
		frmPlayback.getContentPane().add(btn1X);
		
		JButton btn2X = new JButton("2X");
		btn2X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ctx.getCSVReader().setAccelRate(2);
				lblAccel.setText("2X");
			}
		});
		btn2X.setBounds(77, 0, 62, 23);
		frmPlayback.getContentPane().add(btn2X);
		
		JButton btn4X = new JButton("4X");
		btn4X.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ctx.getCSVReader().setAccelRate(4);
				lblAccel.setText("4X");
			}
		});
		btn4X.setBounds(149, 0, 62, 23);
		frmPlayback.getContentPane().add(btn4X);
		
		JButton btn8X = new JButton("8X");
		btn8X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ctx.getCSVReader().setAccelRate(8);
				lblAccel.setText("8X");
			}
		});
		btn8X.setBounds(221, 0, 74, 23);
		frmPlayback.getContentPane().add(btn8X);
		
		
		lblAccel.setBounds(327, 4, 46, 14);
		frmPlayback.getContentPane().add(lblAccel);
		progressBar.setString("");
		
		
		progressBar.setBounds(10, 64, 414, 14);
		frmPlayback.getContentPane().add(progressBar);
		
		frmPlayback.setVisible(true);
	}
}
