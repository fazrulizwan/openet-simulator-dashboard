import java.util.Vector;


public class ChartWindowManager {
	
	private Vector<ChartWindow> chartWindows = new Vector<ChartWindow>();
	private Vector<ChartWin> chartWins = new Vector<ChartWin>();
	private ExplorerWindow explorer;
	private ExplorerWin explorerWin;
	
	public void addChartWindow(ChartWindow win) {		
		chartWindows.add(win);
	}
	
	public void addChartWindow(ChartWin win) {		
		chartWins.add(win);
	}
	
	public Vector<ChartWindow> getChartWindows() {
		return chartWindows;
	}
	
	public Vector<ChartWin> getChartWin() {
		return chartWins;
	}
	
	public void setExplorerWindow(ExplorerWindow explorer) {
		this.explorer = explorer;
	}
	
	public void setExplorerWindow(ExplorerWin explorer) {
		this.explorerWin = explorer;
	}
	
	public ExplorerWindow getExplorerWindow() {
		return explorer;
	}
	
	public ExplorerWin getExplorerWin() {
		return explorerWin;
	}
	
	public void showChartWindows() {
		for(int i=0; i<chartWindows.size(); i++) {
			ChartWindow win = chartWindows.get(i);
			Thread winThread = new Thread(win);
			winThread.start();
		}
	}
}
