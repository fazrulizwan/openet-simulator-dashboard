import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;

import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JButton;

/* Used by InternalFrameDemo.java. */
public class NewChartWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    private JTextField txtChartName;   
	private Context ctx;
	JList listX = new JList();
	JList listY = new JList();
	
	private void addChartWindow(JList listX, JList listY, String chartName) {
		ChartWin win = ctx.getMDI().showChartWin(chartName);
		Color[] color = {Color.GREEN, Color.RED, Color.BLUE, Color.BLACK, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
				
		int[] yChoice = listY.getSelectedIndices();
		for(int i=0; i<yChoice.length; i++) {
			ITrace2D trace1 = new Trace2DLtd(200);			
			trace1.setColor(color[i%color.length]);			
			trace1.setName(ctx.getDataProcessor().getFieldName(yChoice[i]));			
			win.addTrace(trace1, ctx.getDataProcessor().getFieldId(listX.getSelectedIndex()), ctx.getDataProcessor().getFieldId(yChoice[i]));			
		}
		
		win.setAxisYTitle(ctx.getDataProcessor().getUnitName((listY.getSelectedIndex())));
		win.setAxisXTitle(ctx.getDataProcessor().getUnitName((listX.getSelectedIndex())));
		
		ctx.getWindowManager().addChartWindow(win);
		ctx.getWindowManager().getExplorerWin().appendChartWinList(chartName);
	}

    public NewChartWin(Context ctxVal) {
        super("Document #" + (++openFrameCount), 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable

        this.ctx = ctxVal;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(458,283);

        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        getContentPane().setLayout(null);
        
        JLabel label = new JLabel("Chart Name:");
        label.setBounds(10, 14, 70, 14);
        getContentPane().add(label);
        
        txtChartName = new JTextField();
        txtChartName.setColumns(10);
        txtChartName.setBounds(104, 11, 160, 20);
        getContentPane().add(txtChartName);
        
        JLabel label_1 = new JLabel("X-Axis:");
        label_1.setBounds(10, 39, 46, 14);
        getContentPane().add(label_1);
        
        JLabel label_2 = new JLabel("Y-Axis:");
        label_2.setBounds(204, 42, 46, 14);
        getContentPane().add(label_2);
        
        
        listX.setBounds(10, 56, 160, 129);
        getContentPane().add(listX);
        
        
        listY.setBounds(203, 56, 160, 129);
        getContentPane().add(listY);
        
        JButton btnAddChart = new JButton("Add Chart");
        btnAddChart.setBounds(10, 210, 107, 23);
        getContentPane().add(btnAddChart);
        
        Vector<String> dataName = new Vector<String>();
		for(int i=0; i<ctx.getDataProcessor().getStatsData().size(); i++)
			dataName.add(ctx.getDataProcessor().getStatsData().get(i).getFieldName());
		listX.setListData(dataName);
		listY.setListData(dataName);
		
		btnAddChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean conflictYAxisUnit = false;
				Vector<Integer> vecX = new Vector<Integer>();
				Vector<Integer> vecY = new Vector<Integer>();
				
				int[] xChoice = listX.getSelectedIndices();
				for(int i=0; i<xChoice.length; i++) {
					vecX.add(xChoice[i]);
				}
				
				int[] yChoice = listY.getSelectedIndices();
				String yAxisId = "";		
				for(int i=0; i<yChoice.length; i++) {		
					vecY.add(yChoice[i]);
					if(yAxisId.equals("")) {
						yAxisId = ctx.getDataProcessor().getUnitId(yChoice[i]);
					} else {
						if(! ctx.getDataProcessor().getUnitId(yChoice[i]).equalsIgnoreCase(yAxisId)) {
							conflictYAxisUnit = true;
						}
					}
				}
				
				if(txtChartName.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please insert chart name","Chart Name", JOptionPane.WARNING_MESSAGE);
				}				
				else if(listX.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(null, "Please select data for X-Axis","X-Axis", JOptionPane.WARNING_MESSAGE);
				}
				else if(listY.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(null, "Please select data for Y-Axis","Y-Axis", JOptionPane.WARNING_MESSAGE);
				}
				else if(conflictYAxisUnit) {
					JOptionPane.showMessageDialog(null, "The unit of selected Y-Axis data is not same. Select Y-Axis data that have same unit in common","Y-Axis data unit", JOptionPane.WARNING_MESSAGE);
				}
				else {
					ctx.getMDI().addChartWindow(vecX, vecY, txtChartName.getText());
					setVisible(false);
				}				
			}
		});
		
		setVisible(true);
    }
}