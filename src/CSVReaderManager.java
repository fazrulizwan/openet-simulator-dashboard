
public class CSVReaderManager {

	private Context ctx;
	CSVReader reader;
	
	public CSVReaderManager(Context ctx) {
		this.ctx = ctx;
	}
	
	public int establish() {
		reader = new CSVReader(ctx, "C:\\Users\\fazruli\\Documents\\project\\test.csv");
		return 0;
	}
	
	public void setAccelRate(int val) {
		reader.setAccelRate(val);
	}
	
	public void run() {		
		Thread readerTh = new Thread(reader);
		readerTh.start();
	}
	
}
