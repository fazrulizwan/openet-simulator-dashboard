import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class RemoteConnectorReader implements Runnable {

	private Socket sock;
	private boolean running;
	StatsDataProcessor dataProc;
	ChartWindowManager winMan;
	
	public RemoteConnectorReader(Socket sock, StatsDataProcessor dataProc, ChartWindowManager winMan) {
		this.sock = sock;
		running = true;
		this.dataProc = dataProc;
		this.winMan = winMan;
	}
	
	public void run() {
		int byteLen;
		InputStream inMessage = null;
		byte[] receiveBuf = new byte[1024];
		String inMsgStr;
		Object obj;
		JSONObject jsonObj;
		JSONParser parser = new JSONParser();
		String opCode = "";
        int respCode;
        String respMsg;
             
		
		try {
			inMessage = sock.getInputStream();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		while(running) {
			try {
				if((byteLen = inMessage.read(receiveBuf)) != -1)
				{
					inMsgStr = new String(receiveBuf);					
	                inMsgStr = inMsgStr.trim();
	                System.out.println(">>>"+inMsgStr+"<<<");
	                if(inMsgStr.contains("}}"))
	                	System.out.println("");
	                System.out.println(">>>>>>>" + inMsgStr.substring(inMsgStr.length()-2, inMsgStr.length()));
	                if(inMsgStr.substring(inMsgStr.length()-2, inMsgStr.length()).equals("}}"))
	                	inMsgStr = inMsgStr.substring(0, inMsgStr.length()-1);
	                obj = parser.parse(inMsgStr);
	                jsonObj = (JSONObject) obj;
	                System.out.println("DEBUG: jsob obj2: " + jsonObj.toJSONString());
	                
	                if(jsonObj.get("opcode") != null)
	                	opCode = (String) jsonObj.get("opcode");
	                
	                if(opCode.equals("UPDATE_STATS")) {
	                	if(dataProc.processData(jsonObj) == 0) {
	                		for(int i=0; i<winMan.getChartWin().size(); i++) {
	                			ChartWin win = winMan.getChartWin().get(i);
	                			win.updateChart(dataProc);
	                		}	                		
	                	}
	                } else {
	                
		                if(jsonObj.get("response_status") != null)
		                	respCode = Integer.parseInt((String) jsonObj.get("response_status"));
		                
		                if(jsonObj.get("response_message") != null)
		                		respMsg = (String) jsonObj.get("response_message");
	                }
	                
	                
				}
			} catch (IOException e) {				
				e.printStackTrace();
			} catch (ParseException e) {				
				e.printStackTrace();
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}
	}
}
