import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/* Used by InternalFrameDemo.java. */
public class ChangeDataAttrWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    private JTextField txtHiThd;
    private JTextField txtLoThd;
    Context ctx;
    int dataIndex;

    public ChangeDataAttrWin(String dataFieldId, Context ctxVal) {
        super("Document #" + (++openFrameCount), 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable

        this.ctx = ctxVal;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(507,298);

        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        getContentPane().setLayout(null);
        
        JLabel lblUpperThreshold = new JLabel("Upper threshold:");
        lblUpperThreshold.setBounds(37, 73, 96, 14);
        getContentPane().add(lblUpperThreshold);
        
        JLabel lblLowerThreshold = new JLabel("Lower threshold:");
        lblLowerThreshold.setBounds(37, 101, 96, 14);
        getContentPane().add(lblLowerThreshold);
        
        txtHiThd = new JTextField();
        txtHiThd.setBounds(155, 70, 86, 20);
        getContentPane().add(txtHiThd);
        txtHiThd.setColumns(10);
        
        txtLoThd = new JTextField();
        txtLoThd.setBounds(155, 98, 86, 20);
        getContentPane().add(txtLoThd);
        txtLoThd.setColumns(10);
        
        JButton btnOk = new JButton("OK");
        btnOk.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		if(Util.isNumeric(txtHiThd.getText())) {
					ctx.getDataProcessor().setHighThd(dataIndex, Long.valueOf(txtHiThd.getText()));
				}
        		if(Util.isNumeric(txtLoThd.getText())) {
					ctx.getDataProcessor().setLowThd(dataIndex, Long.valueOf(txtLoThd.getText()));
				}
        		if(txtHiThd.getText().trim().equals("")) {
        			ctx.getDataProcessor().setHighThd(dataIndex, Long.MAX_VALUE);
        		}
        		if(txtLoThd.getText().trim().equals("")) {
        			ctx.getDataProcessor().setLowThd(dataIndex, Long.MIN_VALUE);
        		}
				setVisible(false);
        	}
        });
        btnOk.setBounds(87, 177, 89, 23);
        getContentPane().add(btnOk);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setBounds(219, 177, 89, 23);
        getContentPane().add(btnCancel);
        
        dataIndex = -1;
		try {
			dataIndex = ctx.getDataProcessor().getDataIndex(dataFieldId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        Long hiThd = ctx.getDataProcessor().getHighThd(dataIndex);
		Long loThd = ctx.getDataProcessor().getLowThd(dataIndex);
		if(hiThd == Long.MAX_VALUE) {
			txtHiThd.setText("not set");				
		} else {
			txtHiThd.setText(""+hiThd);
		}
		
		if(loThd == Long.MIN_VALUE) {
			txtLoThd.setText("not set");
		} else {
			txtLoThd.setText(""+loThd);
		}
    }
}
