import java.util.Vector;

import org.json.simple.JSONObject;


public class StatsDataProcessor {
	private Vector<StatsData> statsData = new Vector<StatsData>();
	
	public void loadStatsData(int mode) {
		/*
		 * mode 1: xml definition as defined in core sim
		 * mode 2: csv definition
		 */
		if(mode == 1) {
			StatsData data1 = new StatsData();
			data1.setFieldId("ReportElapsedTime");
			data1.setFieldName("Elasped Time");
			data1.setDataType("integer");
			data1.setUnitId("time_sec");
			data1.setUnitName("time (sec)");
			statsData.add(data1);
			
			StatsData data2 = new StatsData();
			data2.setFieldId("ReportCorrelatedTPS");
			data2.setFieldName("TPS");
			data2.setDataType("integer");
			data2.setUnitId("rate_txn_per_sec");
			data2.setUnitName("TPS");
			data2.setHighThd(new Long(9));
			data2.setLowThd(new Long(2));
			statsData.add(data2);		
			
			StatsData data3 = new StatsData();
			data3.setFieldId("ReportMsgSentCumulative");
			data3.setFieldName("Message Sent (Cumulative)");
			data3.setDataType("integer");
			data3.setUnitId("count_messages");
			data3.setUnitName("Messages");
			statsData.add(data3);
			
			StatsData data4 = new StatsData();
			data4.setFieldId("ReportMsgSentPeriodic");
			data4.setFieldName("Message Sent (Periodic)");
			data4.setDataType("integer");
			data4.setUnitId("count_messages");
			data4.setUnitName("Messages");
			statsData.add(data4);
		} else if(mode == 2) {
			StatsData data1 = new StatsData();
			data1.setFieldId("ElapsedTimeMs");
			data1.setFieldName("Elasped Time");
			data1.setDataType("integer");
			data1.setUnitId("time_sec");
			data1.setUnitName("time (sec)");
			statsData.add(data1);
			
			StatsData data2 = new StatsData();
			data2.setFieldId("TPS(C)");
			data2.setFieldName("TPS");
			data2.setDataType("integer");
			data2.setUnitId("rate_txn_per_sec");
			data2.setUnitName("TPS");
			data2.setHighThd(new Long(9));
			data2.setLowThd(new Long(2));
			statsData.add(data2);		
			
			StatsData data3 = new StatsData();
			data3.setFieldId("MsgSent(C)");
			data3.setFieldName("Message Sent (Cumulative)");
			data3.setDataType("integer");
			data3.setUnitId("count_messages");
			data3.setUnitName("Messages");
			statsData.add(data3);
			
			StatsData data4 = new StatsData();
			data4.setFieldId("MsgSent(P)");
			data4.setFieldName("Message Sent (Periodic)");
			data4.setDataType("integer");
			data4.setUnitId("count_messages");
			data4.setUnitName("Messages");
			statsData.add(data4);
			
			StatsData data5 = new StatsData();
			data5.setFieldId("TxnAvgLatUs(P)");
			data5.setFieldName("Average Latency (Periodic)");
			data5.setDataType("integer");
			data5.setUnitId("time_msec");
			data5.setUnitName("millisec");
			statsData.add(data5);
			
			StatsData data6 = new StatsData();
			data6.setFieldId("TxnAvgLatUs(C)");
			data6.setFieldName("Average Latency (Cummulative)");
			data6.setDataType("integer");
			data6.setUnitId("time_msec");
			data6.setUnitName("millisec");
			statsData.add(data6);
			
		}
			
	}
	
	public int processData(JSONObject jsonObj) {
		
		/*
		 * return 0 if success
		 */
				
		int expectedDataCount = Integer.parseInt((String) jsonObj.get("stats_data_count"));		
		
		for(int i=0; i<statsData.size(); i++) {
			String val;
			for(int j=1; j<jsonObj.size(); j++) {
				String fieldId = statsData.get(i).getFieldId();
				val = (String) jsonObj.get(fieldId);
				statsData.get(i).setValue(val);								
			}			
		}
		
		return 0;
	}
	
	public int getDataValue(String fieldId) throws Exception {
		
		for(int i=0; i<statsData.size(); i++) {
			if(statsData.get(i).getFieldId().equalsIgnoreCase(fieldId))
				return statsData.get(i).getValue();
		}
		
		throw new Exception("Field not found in stats data");
	}
	
	public String getFieldIdByName(String fieldName) throws Exception {
		for(int i=0; i<statsData.size(); i++) {
			if(statsData.get(i).getFieldName().equalsIgnoreCase(fieldName))
				return statsData.get(i).getFieldId();
		}
		
		throw new Exception("Field not found in stats data");
	}
	
	public int getDataIndex(String fieldId) throws Exception {
		for(int i=0; i<statsData.size(); i++) {
			if(statsData.get(i).getFieldId().equalsIgnoreCase(fieldId))
				return i;
		}
		
		throw new Exception("Field not found in stats data");		
	}
	
	public int getDataValue(int index) {
		return statsData.get(index).getValue();
	}
		
	public String getFieldId(int index) {		
		
		return statsData.get(index).getFieldId();
	}
	
	public String getUnitName(int index) {
		return statsData.get(index).getUnitName();
	}
	
	public String getUnitId(int index) {
		return statsData.get(index).getUnitId();
	}
	
	public String getFieldName(int index) {
		return statsData.get(index).getFieldName();
	}
	
	public Long getHighThd(int index) {
		return statsData.get(index).getHighThd();
	}
	
	public Long getLowThd(int index) {
		return statsData.get(index).getLowThd();
	}
	
	public Vector<StatsData> getStatsData() {
		return statsData;
	}
	
	public void setHighThd(int index, Long val) {
		statsData.get(index).setHighThd(val);
	}
	
	public void setLowThd(int index, Long val) {
		statsData.get(index).setLowThd(val);
	}
}
