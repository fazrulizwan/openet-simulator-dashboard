import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;
import java.util.Vector;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.TracePoint2D;
import info.monitorenter.gui.chart.pointpainters.PointPainterDisc;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.IAxis.AxisTitle;

/* Used by InternalFrameDemo.java. */
public class ChartWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    private Vector<ITrace2D> traces = new Vector<ITrace2D>();
	private Vector<String> fieldIdX = new Vector<String>();
	private Vector<String> fieldIdY = new Vector<String>();
	Chart2D chart = new Chart2D();
	
	public void updateChart(StatsDataProcessor dataProc) throws Exception {		
		for(int i=0; i<traces.size(); i++) {
			ITrace2D trace = traces.get(i);
			int dataIndexX = dataProc.getDataIndex(fieldIdX.get(i));
			int dataIndexY = dataProc.getDataIndex(fieldIdY.get(i));
			if(dataProc.getDataValue(fieldIdY.get(i)) > dataProc.getLowThd(dataIndexY) && dataProc.getDataValue(fieldIdY.get(i)) < dataProc.getHighThd(dataIndexY)) {
				trace.addPoint(((double) dataProc.getDataValue(dataIndexX)), dataProc.getDataValue(dataIndexY));
			}
			else {
				PointPainterDisc icon = new PointPainterDisc(); 
				icon.setDiscSize(20); // make it bigger than the others			
				icon.setColorFill(Color.RED); // choose a color not used by the others
				TracePoint2D point = new TracePoint2D(dataProc.getDataValue(dataIndexX), dataProc.getDataValue(dataIndexY));
				point.addAdditionalPointPainter(icon);
				trace.addPoint(point);
			}
		}		
	}
	
	public void addTrace(ITrace2D trace, String fieldIdX, String fieldIdY) {
		traces.add(trace);
		this.fieldIdX.add(fieldIdX);
		this.fieldIdY.add(fieldIdY);
		chart.addTrace(trace);
		
	}
	
	public void setAxisXTitle(String title) {
		IAxis axisX = chart.getAxisX();
        axisX.setPaintGrid(true);
        axisX.setAxisTitle(new AxisTitle(title));
	}
	
	public void setAxisYTitle(String titleLeft) {
		IAxis axisY = chart.getAxisY();
        axisY.setPaintGrid(true);
        axisY.setAxisTitle(new AxisTitle(titleLeft));        
	}

    public ChartWin(String boxTitle) {
        super("Document #" + (++openFrameCount), 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
       
        setTitle(boxTitle);
        chart.setBackground(Color.LIGHT_GRAY); 
		chart.setDoubleBuffered(true);
		chart.setUseAntialiasing(true);
	    chart.enablePointHighlighting(true);
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(623,450);

        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        getContentPane().setLayout(null);
        
        
        chart.setUseAntialiasing(true);
        chart.setDoubleBuffered(true);
        chart.setBackground(Color.LIGHT_GRAY);
        chart.setBounds(0, 0, 607, 410);
        getContentPane().add(chart);
    }
    
    
}