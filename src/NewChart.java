import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.ListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class NewChart {

	private JFrame frmNewChart;
	private JTextField txtChartName;
	private Context ctx;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//NewChart window = new NewChart();
					//window.frmNewChart.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public NewChart(Context ctx) {
		this.ctx = ctx;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		final JList listY = new JList();
		final JList listX = new JList();
		
		frmNewChart = new JFrame();
		frmNewChart.setTitle("New Chart");
		frmNewChart.setBounds(100, 100, 450, 300);
		frmNewChart.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNewChart.getContentPane().setLayout(null);		
		
		listX.setModel(new AbstractListModel() {
			String[] values = new String[] {"dfsds"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listX.setBounds(10, 53, 160, 129);
		frmNewChart.getContentPane().add(listX);
		
		JLabel lblChartName = new JLabel("Chart Name:");
		lblChartName.setBounds(10, 11, 70, 14);
		frmNewChart.getContentPane().add(lblChartName);
		
		txtChartName = new JTextField();
		txtChartName.setBounds(104, 8, 160, 20);
		frmNewChart.getContentPane().add(txtChartName);
		txtChartName.setColumns(10);
		
		JLabel lblXaxis = new JLabel("X-Axis:");
		lblXaxis.setBounds(10, 36, 46, 14);
		frmNewChart.getContentPane().add(lblXaxis);
		
		frmNewChart.setVisible(true);
		
		Vector<String> dataName = new Vector<String>();
		for(int i=0; i<ctx.getDataProcessor().getStatsData().size(); i++)
			dataName.add(ctx.getDataProcessor().getStatsData().get(i).getFieldName());
		listX.setListData(dataName);
		listY.setListData(dataName);
		
		JLabel lblYaxis = new JLabel("Y-Axis:");
		lblYaxis.setBounds(204, 39, 46, 14);
		frmNewChart.getContentPane().add(lblYaxis);
		
		
		listY.setBounds(203, 53, 160, 129);
		frmNewChart.getContentPane().add(listY);
		
		
		
		JButton btnAddChart = new JButton("Add Chart");
		btnAddChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean conflictYAxisUnit = false;
				
				int[] yChoice = listY.getSelectedIndices();
				String yAxisId = "";		
				for(int i=0; i<yChoice.length; i++) {			
					if(yAxisId.equals("")) {
						yAxisId = ctx.getDataProcessor().getUnitId(yChoice[i]);
					} else {
						if(! ctx.getDataProcessor().getUnitId(yChoice[i]).equalsIgnoreCase(yAxisId)) {
							conflictYAxisUnit = true;
						}
					}
				}
				
				if(txtChartName.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please insert chart name","Chart Name", JOptionPane.WARNING_MESSAGE);
				}				
				else if(listX.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(null, "Please select data for X-Axis","X-Axis", JOptionPane.WARNING_MESSAGE);
				}
				else if(listY.getSelectedIndex() == -1) {
					JOptionPane.showMessageDialog(null, "Please select data for Y-Axis","Y-Axis", JOptionPane.WARNING_MESSAGE);
				}
				else if(conflictYAxisUnit) {
					JOptionPane.showMessageDialog(null, "The unit of selected Y-Axis data is not same. Select Y-Axis data that have same unit in common","Y-Axis data unit", JOptionPane.WARNING_MESSAGE);
				}
				else {
					addChartWindow(listX, listY, txtChartName.getText());
					frmNewChart.setVisible(false);
				}				
			}
		});
		btnAddChart.setBounds(10, 207, 107, 23);
		frmNewChart.getContentPane().add(btnAddChart);
		
	}
	
	private void addChartWindow(JList listX, JList listY, String chartName) {
		ChartWindow win = new ChartWindow(chartName);
		Color[] color = {Color.GREEN, Color.RED, Color.BLUE, Color.BLACK, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
				
		int[] yChoice = listY.getSelectedIndices();
		for(int i=0; i<yChoice.length; i++) {
			ITrace2D trace1 = new Trace2DLtd(200);			
			trace1.setColor(color[i%color.length]);			
			trace1.setName(ctx.getDataProcessor().getFieldName(yChoice[i]));			
			win.addTrace(trace1, ctx.getDataProcessor().getFieldId(listX.getSelectedIndex()), ctx.getDataProcessor().getFieldId(yChoice[i]));			
		}
		
		win.setAxisYTitle(ctx.getDataProcessor().getUnitName((listY.getSelectedIndex())));
		win.setAxisXTitle(ctx.getDataProcessor().getUnitName((listX.getSelectedIndex())));
		
		ctx.getWindowManager().addChartWindow(win);
		ctx.getWindowManager().getExplorerWindow().appendChartWinList(chartName);
	}
}
