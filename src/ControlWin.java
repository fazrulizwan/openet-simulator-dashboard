import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JLabel;
import javax.swing.JTextField;

/* Used by InternalFrameDemo.java. */
public class ControlWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    Context ctx;
    JLabel lblSimRun = new JLabel("...");
    JButton btnPlay = new JButton("");
    JButton btnStop = new JButton("");
    JButton btnConnect = new JButton("Connect");
    JLabel label = new JLabel("...");
    JLabel lblSimName = new JLabel("...");
    private final JLabel lblHost = new JLabel("Host:");
    private final JTextField txtHost = new JTextField();
    private final JLabel lblPort = new JLabel("Port:");
    private final JTextField txtPort = new JTextField();

    public ControlWin(Context ctxVal) {
        super("Simulator Control", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        txtPort.setBounds(188, 8, 86, 20);
        txtPort.setColumns(10);
        txtHost.setBounds(50, 8, 86, 20);
        txtHost.setColumns(10);
        
        this.ctx = ctxVal;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(556,286);

        //Set the window's location.
        setLocation(ctx.getMDI().getWidth()/4, ctx.getMDI().getHeight()/4);
        getContentPane().setLayout(null);
        
        
        btnConnect.setBounds(302, 7, 160, 23);
        getContentPane().add(btnConnect);
        
        
        btnPlay.setEnabled(false);
        btnPlay.setBounds(10, 81, 53, 53);
        getContentPane().add(btnPlay);
        
       
        btnStop.setEnabled(false);
        btnStop.setBounds(64, 79, 55, 55);
        getContentPane().add(btnStop);
        
        
        lblSimRun.setBounds(129, 95, 346, 23);
        getContentPane().add(lblSimRun);
        
        
        label.setOpaque(true);
        label.setBounds(23, 38, 439, 23);
        getContentPane().add(label);
        
        
        lblSimName.setBounds(50, 195, 424, 23);
        getContentPane().add(lblSimName);
        
        btnPlay.setEnabled(false);
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ctx.getMode() == 1) {
					if(ctx.getRemoteConnector().run() == 0) {					
						btnPlay.setEnabled(false);
						btnStop.setEnabled(true);
						lblSimRun.setText("Simulator is running");
					}
				} else if(ctx.getMode() == 2) {
					ctx.getCSVReader().run();
				}
			}
		});
		
		btnPlay.setIcon(new ImageIcon("C:\\Users\\fazruli\\Documents\\JIRA-WS\\jchart_project\\images\\play_button.gif"));				
		btnStop.setEnabled(false);
		btnStop.setIcon(new ImageIcon("C:\\Users\\fazruli\\Documents\\JIRA-WS\\jchart_project\\images\\StopIcon.jpg"));
		lblHost.setBounds(10, 11, 33, 14);
		
		getContentPane().add(lblHost);
		
		getContentPane().add(txtHost);
		lblPort.setBounds(146, 11, 32, 14);
		
		getContentPane().add(lblPort);
		
		getContentPane().add(txtPort);
		
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ctx.getMode() == 1) {
					if(txtHost.getText().trim().equals("")) {
						JOptionPane.showMessageDialog(null, "Please insert host","Host", JOptionPane.WARNING_MESSAGE);
						return;
					}
					if(txtPort.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Please insert port","port", JOptionPane.WARNING_MESSAGE);
						return;
					}
					RemoteConnector con = new RemoteConnector(txtHost.getText(), Integer.parseInt(txtPort.getText()), ctx.getDataProcessor(), ctx.getWindowManager(), ctx);
					ctx.setRemoteConnector(con);
					ctx.setSimHost(txtHost.getText());
					ctx.setSimPort(Integer.valueOf(txtPort.getText()));
					int estStatus = ctx.getRemoteConnector().establish();
					if(estStatus == 0) {
						ctx.setSimServConnected(true);
						label.setText("Connected to simulator");
						label.setBackground(Color.GREEN);
						btnConnect.setEnabled(false);
						lblSimName.setText("Simulator: [" + ctx.getSimulatorName() + "] [Ver: "+ctx.getSimulatorVersion()+"]");
						lblSimRun.setText("simulator is not running");
						btnPlay.setEnabled(true);
						ctx.getWindowManager().showChartWindows();
					} else {
						JOptionPane.showMessageDialog(null, "Fail to connect to the simulator","Connection fail", JOptionPane.WARNING_MESSAGE);
						return;
					}
						
				} else if(ctx.getMode() == 2) {
					label.setBackground(Color.RED);
					label.setText("Loading playback data...");
					if(ctx.getCSVReader().establish() == 0) {
						label.setText("Playback mode ready");
						label.setBackground(Color.GREEN);
						btnConnect.setEnabled(false);
						//lblSimName.setText("Simulator: [" + ctx.getSimulatorName() + "] [Ver: "+ctx.getSimulatorVersion()+"]");
						lblSimRun.setText("playback stopped");
						btnPlay.setEnabled(true);
						ctx.getWindowManager().showChartWindows();
					}
				}
				
			}
		});
		
		if(ctx.isSimServConnected()) {
			label.setText("Connected to simulator");
			label.setBackground(Color.GREEN);
		}
		else {
			label.setText("Not connected to simulator");
			label.setBackground(Color.RED);			
		}
    }
}