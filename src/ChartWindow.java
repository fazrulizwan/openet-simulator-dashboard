import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.IAxisScalePolicy;
import info.monitorenter.gui.chart.IRangePolicy;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.TracePoint2D;
import info.monitorenter.gui.chart.axis.AAxis;
import info.monitorenter.gui.chart.axis.AxisLinear;
import info.monitorenter.gui.chart.axis.scalepolicy.AxisScalePolicyManualTicks;
import info.monitorenter.gui.chart.pointpainters.PointPainterDisc;
import info.monitorenter.gui.chart.pointpainters.PointPainterLine;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JFrame;


public class ChartWindow implements Runnable {

	private JFrame frame;
	private Vector<ITrace2D> traces = new Vector<ITrace2D>();
	private Vector<String> fieldIdX = new Vector<String>();
	private Vector<String> fieldIdY = new Vector<String>();
	
	Chart2D chart;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//ChartWindow window = new ChartWindow();
					//window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public ChartWindow(String boxTitle) {		
		initialize();
		frame.setTitle(boxTitle);
	}
	
	public void run() {
		//initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {		
		chart = new Chart2D();
		frame = new JFrame();
		frame.setBounds(100, 100, 608, 392);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(chart);
		frame.setVisible(true);		
		chart.setBackground(Color.LIGHT_GRAY); 
		chart.setDoubleBuffered(true);
		chart.setUseAntialiasing(true);
	    chart.enablePointHighlighting(true);
	    
        
	    
	    //Timer timer = new Timer(true);
	    //TimerTask task = new TimerTask(){

	      //private double m_y = 0;
	      //private long m_starttime = System.currentTimeMillis();
	      /**
	       * @see java.util.TimerTask#run()
	       */
	      //@Override
	      //public void run() {
	        // This is just computation of some nice looking value.
	       // double rand = Math.random();
	       // boolean add = (rand >= 0.5) ? true : false;
	        //this.m_y = (add) ? this.m_y + Math.random() : this.m_y - Math.random();
	        // This is the important thing: Point is added from separate Thread.
	        //trace.addPoint(((double) System.currentTimeMillis() - this.m_starttime), this.m_y);
	        
	            		
	      //}
	      
	    //};
	    // Every 20 milliseconds a new value is collected.
	    //timer.schedule(task, 1000, 1000);
	}
	
	public void updateChart(StatsDataProcessor dataProc) throws Exception {		
		for(int i=0; i<traces.size(); i++) {
			ITrace2D trace = traces.get(i);
			int dataIndexX = dataProc.getDataIndex(fieldIdX.get(i));
			int dataIndexY = dataProc.getDataIndex(fieldIdY.get(i));
			if(dataProc.getDataValue(fieldIdY.get(i)) > dataProc.getLowThd(dataIndexY) && dataProc.getDataValue(fieldIdY.get(i)) < dataProc.getHighThd(dataIndexY)) {
				trace.addPoint(((double) dataProc.getDataValue(dataIndexX)), dataProc.getDataValue(dataIndexY));
			}
			else {
				PointPainterDisc icon = new PointPainterDisc(); 
				icon.setDiscSize(20); // make it bigger than the others			
				icon.setColorFill(Color.RED); // choose a color not used by the others
				TracePoint2D point = new TracePoint2D(dataProc.getDataValue(dataIndexX), dataProc.getDataValue(dataIndexY));
				point.addAdditionalPointPainter(icon);
				trace.addPoint(point);
			}
		}		
	}
	
	public void addTrace(ITrace2D trace, String fieldIdX, String fieldIdY) {
		traces.add(trace);
		this.fieldIdX.add(fieldIdX);
		this.fieldIdY.add(fieldIdY);
		chart.addTrace(trace);
		
	}
	
	public void setAxisXTitle(String title) {
		IAxis axisX = chart.getAxisX();
        axisX.setPaintGrid(true);
        axisX.setAxisTitle(new AxisTitle(title));
	}
	
	public void setAxisYTitle(String titleLeft) {
		IAxis axisY = chart.getAxisY();
        axisY.setPaintGrid(true);
        axisY.setAxisTitle(new AxisTitle(titleLeft));        
	}

}
