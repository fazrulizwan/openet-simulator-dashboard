import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;


public class ExplorerWindow  {

	private JFrame frame;
	final JTree treeData = new JTree();
	private Context ctx;
	private DefaultMutableTreeNode node_1;
	private Vector<DefaultMutableTreeNode> chartWinNodes = new Vector<DefaultMutableTreeNode>();
	private Vector<DefaultMutableTreeNode> statsDataNodes = new Vector<DefaultMutableTreeNode>();
	
	final JTree treeDB = new JTree();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//ExplorerWindow window = new ExplorerWindow();
					//window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public ExplorerWindow(Context ctx) {
		this.ctx = ctx;
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		final JMenu mnNew = new JMenu("New");
		
		frame = new JFrame();
		frame.setBounds(100, 100, 306, 563);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Project");
		mnNewMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmOpenFile = new JMenuItem("Connect to simulator..");
		mntmOpenFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		mnNewMenu.add(mntmOpenFile);
		
		JMenuItem mntmPlayback = new JMenuItem("Playback from data file..");
		mnNewMenu.add(mntmPlayback);
		
		
		treeData.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(arg0.getClickCount() == 2) {
					TreePath tp = treeData.getPathForLocation(arg0.getX(), arg0.getY());					
					int depth = tp.getPathCount();
					
					if(depth == 2) {
						Object[] pathNode = tp.getPath();
						String fieldName = pathNode[1].toString().trim();
						//if(pathNode[1].toString().trim().equalsIgnoreCase("TPS")) {
						//	ctx.getDataProcessor().
						//}
						try {
							ChangeDataAttribute changeDataAttrWin = new ChangeDataAttribute(ctx.getDataProcessor().getFieldIdByName(fieldName), ctx);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
					
					//System.out.println("path count:"  + tp.getPathCount());
					//System.out.println("parent:" + tp.getParentPath().toString());
					
					//System.out.println(pathNode[0].toString() + pathNode[1].toString() + pathNode[2].toString());
				}
			}
		});
		treeData.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Data") {
				{
					for(int i=0; i<ctx.getDataProcessor().getStatsData().size(); i++) {
						DefaultMutableTreeNode statsDataNode = new DefaultMutableTreeNode(ctx.getDataProcessor().getStatsData().get(i).getFieldName());
						statsDataNodes.add(statsDataNode);
						add(statsDataNode);
						/*
						DefaultMutableTreeNode statsDataHighThdNode;
						DefaultMutableTreeNode statsDataLowThdNode;
						if(ctx.getDataProcessor().getStatsData().get(i).getHighThd() == Long.MAX_VALUE) {
							statsDataHighThdNode = new DefaultMutableTreeNode("High threshold: not set");
						}
						else {
							statsDataHighThdNode = new DefaultMutableTreeNode("High threshold:" + ctx.getDataProcessor().getStatsData().get(i).getHighThd());
						}
						statsDataNode.add(statsDataHighThdNode);
						
						if(ctx.getDataProcessor().getStatsData().get(i).getLowThd() == Long.MIN_VALUE) {
							statsDataLowThdNode = new DefaultMutableTreeNode("Low threshold: not set");
						}
						else {
							statsDataLowThdNode = new DefaultMutableTreeNode("Low threshold:" + ctx.getDataProcessor().getStatsData().get(i).getLowThd());
						}
						statsDataNode.add(statsDataLowThdNode);
						*/
					}
					
					/*
					add(new DefaultMutableTreeNode("Elapsed Time"));
					add(new DefaultMutableTreeNode("Message Sent (Periodic)"));
					add(new DefaultMutableTreeNode("Message Sent (Cumulative)"));
					add(new DefaultMutableTreeNode("Message Received (Periodic)"));
					add(new DefaultMutableTreeNode("Message Received (Cumulative)"));
					add(new DefaultMutableTreeNode("TPS")); */
				}
			}
		));
		frame.getContentPane().add(treeData, BorderLayout.NORTH);
		
		
		treeDB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(arg0.getButton() == arg0.BUTTON3) {
					//treeData.setVisible(false);
					//mnNew.setVisible(true);
				} 
				if(arg0.getClickCount() == 2) {
					NewChart newChart = new NewChart(ctx);
					
				}
			}
		});
		
		treeDB.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Chart") {
				{
					//DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("Chart Window");
						node_1.add(new DefaultMutableTreeNode("Add Chart..."));
					add(node_1);
					//node_1 = new DefaultMutableTreeNode("Report");
					//	node_1.add(new DefaultMutableTreeNode("Add Report..."));
					//add(node_1);
				}
			}
		));
		frame.getContentPane().add(treeDB, BorderLayout.CENTER);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(treeDB, popupMenu);
		
		popupMenu.add(mnNew);
		
		JMenuItem mntmChart = new JMenuItem("Chart");
		mntmChart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NewChart newChart = new NewChart(ctx);				
			}
		});
		mnNew.add(mntmChart);
		
		frame.setVisible(true);
		//this.
	}
	
	public void appendChartWinList(String name) {
		DefaultMutableTreeNode chartWinNode = new DefaultMutableTreeNode(name);		
		node_1.add(chartWinNode);
		chartWinNode.add(new DefaultMutableTreeNode("X-Axis"));
		treeDB.updateUI();
		//frame.repaint();
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}	
	
	private void addChartNode(String nodeName) {
		
	}
}
