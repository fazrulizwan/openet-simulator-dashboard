import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
 
public class ProjectLoader {
	
	private Document doc;
	Context ctx;
	
	public ProjectLoader(Context ctxVal) {
		this.ctx = ctxVal;
	}
	
	public int load() {
 
		File fXmlFile = new File("c:\\temp\\dashboardproject1.dbp");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		loadChartWin();
		
	  
	  return 0;
  }
	
	private void loadChartWin() {
		
		
		
		try {			
	 
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("chartwindow");
			System.out.println("-----------------------");
	 
			for (int temp = 0; temp < nList.getLength(); temp++) {
				
				Vector<Integer> vecX = new Vector<Integer>();
				Vector<Integer> vecY = new Vector<Integer>();
	 
			   Node nNode = nList.item(temp);
			   if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
			      Element eElement = (Element) nNode;
			      
			      String chartName = getTagValue("name", eElement);
			      System.out.println("name : " + getTagValue("name", eElement));
			      
			      int chartWinCountX = getTagCount("xfieldid", eElement);
			      System.out.println("XCount : " + chartWinCountX);
			      for(int i=0; i<chartWinCountX; i++) {
			    	  System.out.println("Xaxis : " + getTagValue("xfieldid", eElement, i));
			    	  String xFieldId = getTagValue("xfieldid", eElement, i);
			    	  for(int j=0; j<ctx.getDataProcessor().getStatsData().size(); j++) {
				    	  if(xFieldId.equalsIgnoreCase(ctx.getDataProcessor().getFieldId(j))) {
				    		  vecX.add(j);
				    	  }
				      }
			      }
	 
			      int chartWinCountY = getTagCount("yfieldid", eElement);
			      System.out.println("YCount : " + chartWinCountY);
			      for(int i=0; i<chartWinCountY; i++) {
			    	  System.out.println("Yaxis : " + getTagValue("yfieldid", eElement, i));
			    	  String yFieldId = getTagValue("yfieldid", eElement, i);
			    	  for(int j=0; j<ctx.getDataProcessor().getStatsData().size(); j++) {
				    	  if(yFieldId.equalsIgnoreCase(ctx.getDataProcessor().getFieldId(j))) {
				    		  vecY.add(j);
				    	  }
				      }
			      }
			      
			      
			      
			      ctx.getMDI().addChartWindow(vecX, vecY, chartName);
			   }
			}
		  } catch (Exception e) {
			e.printStackTrace();
		  }
		
	}
	
 private static int getTagCount(String sTag, Element eElement) {
		return eElement.getElementsByTagName(sTag).getLength();
	 
	       
 }
 
 private static String getTagValue(String sTag, Element eElement, int index) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(index).getChildNodes();
	 
	        Node nValue = (Node) nlList.item(0);
	 
		return nValue.getNodeValue();
	  }
 
  private static String getTagValue(String sTag, Element eElement) {
	NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
 
        Node nValue = (Node) nlList.item(0);
 
	return nValue.getNodeValue();
  }
 
}