
 
import javax.swing.JInternalFrame;
 
import java.awt.event.*;
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
 
/* Used by InternalFrameDemo.java. */
public class PlayBackWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    JLabel lblAccel = new JLabel("1X");
    Context ctx;
    JProgressBar progressBar = new JProgressBar();
    
    public void setProgress(int val) {
		progressBar.setValue(val);		
	}
	
	public void setProgressMax(int val) {
		progressBar.setMaximum(val);		
	}
 
    public PlayBackWin(Context ctxVal) {
        super("Document #" + (++openFrameCount), 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable
        this.ctx = ctxVal;
        //...Create the GUI and put it in the window...
 
        //...Then set the window size or call pack...
        setSize(474,159);
 
        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        getContentPane().setLayout(null);
        
        JButton btn1X = new JButton("1X");
        btn1X.setBounds(34, 27, 57, 23);
        getContentPane().add(btn1X);
        
        JButton btn2X = new JButton("2X");
        btn2X.setBounds(101, 27, 62, 23);
        getContentPane().add(btn2X);
        
        JButton btn4X = new JButton("4X");
        btn4X.setBounds(173, 27, 62, 23);
        getContentPane().add(btn4X);
        
        JButton btn8X = new JButton("8X");
        btn8X.setBounds(245, 27, 74, 23);
        getContentPane().add(btn8X);
        
        
        lblAccel.setBounds(351, 31, 46, 14);
        getContentPane().add(lblAccel);
        
        
        progressBar.setString("");
        progressBar.setBounds(34, 91, 414, 14);
        getContentPane().add(progressBar);
        
        btn1X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ctx.getCSVReader().setAccelRate(1);
				lblAccel.setText("1X");
			}
		});
        
        btn2X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ctx.getCSVReader().setAccelRate(2);
				lblAccel.setText("2X");
			}
		});
        
        btn4X.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ctx.getCSVReader().setAccelRate(4);
				lblAccel.setText("4X");
			}
		});
        
        btn8X.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ctx.getCSVReader().setAccelRate(8);
				lblAccel.setText("8X");
			}
		});
    }
}