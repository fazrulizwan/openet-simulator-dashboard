import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/* Used by InternalFrameDemo.java. */
public class ConnSimWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    private JTextField txtHost;
    private JTextField txtPort;
    Context ctx;

    public ConnSimWin(Context ctxVal) {
        super("Connect to Simulator", 
              true, //resizable
              true, //closable
              true, //maximizable
              true);//iconifiable

        this.ctx = ctxVal;
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(402,234);

        //Set the window's location.
        setLocation(ctx.getMDI().getWidth()/4, ctx.getMDI().getHeight()/4);
        getContentPane().setLayout(null);
        
        JLabel lblHost = new JLabel("host:");
        lblHost.setBounds(38, 45, 46, 14);
        getContentPane().add(lblHost);
        
        JLabel lblPort = new JLabel("port:");
        lblPort.setBounds(38, 71, 46, 14);
        getContentPane().add(lblPort);
        
        txtHost = new JTextField();
        txtHost.setBounds(72, 42, 86, 20);
        getContentPane().add(txtHost);
        txtHost.setColumns(10);
        
        txtPort = new JTextField();
        txtPort.setBounds(72, 70, 86, 20);
        getContentPane().add(txtPort);
        txtPort.setColumns(10);
        
        JButton btnOk = new JButton("OK");
        btnOk.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		ctx.setSimHost(txtHost.getText());
        		ctx.setSimPort(Integer.valueOf(txtPort.getText()));
        		ctx.setMode(1);
        		ctx.getMDI().showControlWin();
        		ctx.getDataProcessor().loadStatsData(1);
        		ctx.getMDI().refreshExplorer();
        		setVisible(false);
        	}
        });
        btnOk.setBounds(142, 131, 89, 23);
        getContentPane().add(btnOk);
    }
}