import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Button;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ChangeDataAttribute {

	private JFrame frnChangeDataAttr;
	private Label lblAttrName = new Label("High Threshold");
	private Label label = new Label("Low Threshold:");
	private TextField txtHiThd = new TextField();
	private TextField txtLoThd = new TextField();
	private Context ctx;
	int dataIndex;

	/**
	 * Create the application.
	 */
	public ChangeDataAttribute(String dataFieldId, Context ctxVal) {
		this.ctx = ctxVal;
		initialize();
		dataIndex = -1;
		try {
			dataIndex = ctx.getDataProcessor().getDataIndex(dataFieldId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Long hiThd = ctx.getDataProcessor().getHighThd(dataIndex);
		Long loThd = ctx.getDataProcessor().getLowThd(dataIndex);
		if(hiThd == Long.MAX_VALUE) {
			txtHiThd.setText("not set");				
		} else {
			txtHiThd.setText(""+hiThd);
		}
		
		if(loThd == Long.MIN_VALUE) {
			txtLoThd.setText("not set");
		} else {
			txtLoThd.setText(""+loThd);
		}
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frnChangeDataAttr = new JFrame();
		frnChangeDataAttr.setBounds(100, 100, 450, 294);
		frnChangeDataAttr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frnChangeDataAttr.getContentPane().setLayout(null);
		
		
		lblAttrName.setBounds(45, 65, 89, 22);
		frnChangeDataAttr.getContentPane().add(lblAttrName);
		
		
		txtHiThd.setBounds(140, 65, 70, 22);
		frnChangeDataAttr.getContentPane().add(txtHiThd);
		
		txtLoThd.setBounds(140, 93, 70, 22);
		frnChangeDataAttr.getContentPane().add(txtLoThd);
		
		
		label.setBounds(45, 93, 89, 22);
		frnChangeDataAttr.getContentPane().add(label);
		
		Button btnOK = new Button("OK");
		btnOK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(Util.isNumeric(txtHiThd.getText())) {
					ctx.getDataProcessor().setHighThd(dataIndex, Long.valueOf(txtHiThd.getText()));
				}
				
				frnChangeDataAttr.setVisible(false);
			}
		});
		btnOK.setBounds(46, 193, 70, 22);
		frnChangeDataAttr.getContentPane().add(btnOK);
		
		Button btnCancel = new Button("Cancel");
		btnCancel.setBounds(176, 193, 70, 22);
		frnChangeDataAttr.getContentPane().add(btnCancel);
		
		frnChangeDataAttr.setVisible(true);
		
	}
}
