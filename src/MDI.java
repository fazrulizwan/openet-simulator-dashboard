/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 



import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;

import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import java.awt.event.*;
import java.awt.*;
import java.io.File;
import java.util.Vector;

/*
 * InternalFrameDemo.java requires:
 *   MyInternalFrame.java
 */
public class MDI extends JFrame
                               implements ActionListener {
    private JDesktopPane desktop;
    private Context ctx;
    ExplorerWin expWin;
    JFileChooser fc = new JFileChooser();
    ChangeDataAttrWin changeAttr;
    
    public MDI(Context ctx) {
        super("Simulator Dashboard");
        addWindowListener(new WindowAdapter() {
        	@Override
        	public void windowClosed(WindowEvent arg0) {
        		System.exit(0);
        	}
        	@Override
        	public void windowDeactivated(WindowEvent e) {
        		
        	}
        });
        
        this.ctx = ctx;
        //Make the big window be indented 50 pixels from each edge
        //of the screen.
        int inset = 50;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(inset, inset,
                  screenSize.width  - inset*2,
                  screenSize.height - inset*2);

        //Set up the GUI.
        desktop = new JDesktopPane(); //a specialized layered pane
        createFrame(); //create first "window"
        setContentPane(desktop);
        setJMenuBar(createMenuBar());

        //Make dragging a little faster but perhaps uglier.
        desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
    }

    protected JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        //Set up the lone menu.
        JMenu menu = new JMenu("Project");
        menu.setMnemonic(KeyEvent.VK_D);
        menuBar.add(menu);

        //Set up the first menu item.
        JMenuItem menuItem = new JMenuItem("Connect to simulator...");
        menuItem.setMnemonic(KeyEvent.VK_N);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("connectsim");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //Set up the second menu item.
        menuItem = new JMenuItem("Read from file...");
        menuItem.setMnemonic(KeyEvent.VK_Q);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("readfile");
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Load project...");
        menuItem.setMnemonic(KeyEvent.VK_P);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_P, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("loadproject");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        return menuBar;
    }

    //React to menu selections.
    public void actionPerformed(ActionEvent e) {
        if ("connectsim".equals(e.getActionCommand())) { //new
            //ConnSimWin conSim = new ConnSimWin(ctx);
            //conSim.setVisible(true);
            //desktop.add(conSim);
        	ControlWin ctrl = new ControlWin(ctx);
        	ctrl.setVisible(true);
        	desktop.add(ctrl);
        } else if("readfile".equals(e.getActionCommand())) {
        	int returnVal = fc.showOpenDialog(MDI.this);        	
        	if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                System.out.println("Opening: " + file.getName());
                ctx.setMode(2);
                CSVReaderManager csvReader = new CSVReaderManager(ctx);
            	ctx.setCSVReader(csvReader);
                ControlWin ctrl = new ControlWin(ctx);
                ctrl.setVisible(true);
                desktop.add(ctrl);
                PlayBackWin pb = new PlayBackWin(ctx);
                ctx.setPlayBack(pb);
                pb.setVisible(true);
                desktop.add(pb);
                ctx.getDataProcessor().loadStatsData(2);
        		refreshExplorer();
            } else {
            	System.out.println("Open command cancelled by user.");
            }
        } else if("loadproject".equals(e.getActionCommand())) {
        	ProjectLoader prLoad = new ProjectLoader(ctx);
    		prLoad.load();
        } else { //quit
            //quit();
        	//ExplorerWindow explorer = new ExplorerWindow(ctx);
        	//explorer.setVisible(true);
        	//desktop.add(explorer);
        }
    }

    //Create a new internal frame.
    protected void createFrame() {
        
        expWin = new ExplorerWin(ctx);        
        expWin.setVisible(true);        
        desktop.add(expWin);
        ctx.getWindowManager().setExplorerWindow(expWin);
        
    }
    
    public void showNewChartWin(Context ctx) {
    	NewChartWin newChart = new NewChartWin(ctx);
    	newChart.setVisible(true);
    	desktop.add(newChart);
    }
    
    public ChartWin showChartWin(String chartName) {
    	ChartWin win = new ChartWin(chartName);
    	win.setVisible(true);
    	desktop.add(win);
    	return win;
    }
    
    public void showControlWin() {
    	ControlWin ctrl = new ControlWin(ctx);
    	ctrl.setVisible(true);
    	desktop.add(ctrl);
    }
    
    public void showChangeAttrWin(String dataFieldId, Context ctx) {
    	changeAttr = new ChangeDataAttrWin(dataFieldId, ctx);
    	changeAttr.setVisible(true);
    	desktop.add(changeAttr);
    }
    
    public void refreshExplorer() {
    	expWin.dispose();
    	expWin = new ExplorerWin(ctx);        
        expWin.setVisible(true);        
        desktop.add(expWin);
        ctx.getWindowManager().setExplorerWindow(expWin);
    }

    //Quit the application.
    protected void quit() {
        System.exit(0);
    }
    
    public void addChartWindow(Vector<Integer> listX, Vector<Integer> listY, String chartName) {
		ChartWin win = ctx.getMDI().showChartWin(chartName);
		Color[] color = {Color.GREEN, Color.RED, Color.BLUE, Color.BLACK, Color.PINK, Color.CYAN, Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.YELLOW};
				
		for(int i=0; i<listY.size(); i++) {
			ITrace2D trace1 = new Trace2DLtd(200);			
			trace1.setColor(color[i%color.length]);			
			trace1.setName(ctx.getDataProcessor().getFieldName(listY.get(i)));			
			win.addTrace(trace1, ctx.getDataProcessor().getFieldId(listX.get(i)), ctx.getDataProcessor().getFieldId(listY.get(i)));			
		}
		
		win.setAxisYTitle(ctx.getDataProcessor().getUnitName((listY.get(0))));
		win.setAxisXTitle(ctx.getDataProcessor().getUnitName((listX.get(0))));
		
		ctx.getWindowManager().addChartWindow(win);
		ctx.getWindowManager().getExplorerWin().appendChartWinList(chartName);
	}

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    /*
    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        InternalFrameDemo frame = new InternalFrameDemo();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Display the window.
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
    */
}