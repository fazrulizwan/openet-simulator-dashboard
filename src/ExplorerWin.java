import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;
import java.io.File;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/* Used by InternalFrameDemo.java. */
public class ExplorerWin extends JInternalFrame {
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    final JTree treeData = new JTree();
	private Context ctx;
	private DefaultMutableTreeNode node_1;
	private Vector<DefaultMutableTreeNode> chartWinNodes = new Vector<DefaultMutableTreeNode>();
	private Vector<DefaultMutableTreeNode> statsDataNodes = new Vector<DefaultMutableTreeNode>();
	 JTree treeDB = new JTree();
	JFileChooser fc = new JFileChooser();
	
	public void appendChartWinList(String name) {
		DefaultMutableTreeNode chartWinNode = new DefaultMutableTreeNode(name);		
		node_1.add(chartWinNode);
		chartWinNode.add(new DefaultMutableTreeNode("X-Axis"));
		treeDB.updateUI();
		//frame.repaint();
	}
	
    public ExplorerWin(Context ctxVal) {
        super("Explorer", 
              false, //resizable
              false, //closable
              false, //maximizable
              false);//iconifiable

        this.ctx = ctxVal;
        this.add(fc);
        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(301,546);

        //Set the window's location.
        setLocation(0, 0);
        getContentPane().setLayout(null);
        initialize();
        
    }
    
    private void initialize() {
    	
    	JMenuBar menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, 290, 21);
        getContentPane().add(menuBar);
        
        
        treeData.setModel(new DefaultTreeModel(
        	new DefaultMutableTreeNode("Data") {
        		{
        		}
        	}
        ));
        treeData.setBounds(0, 21, 290, 156);
        getContentPane().add(treeData);
        
       
        treeDB.setModel(new DefaultTreeModel(
        	new DefaultMutableTreeNode("Chart") {
        		{
        			DefaultMutableTreeNode node_1;
        			node_1 = new DefaultMutableTreeNode("Chart Window");
        				node_1.add(new DefaultMutableTreeNode("Add Chart..."));
        			add(node_1);
        		}
        	}
        ));
        treeDB.setBounds(0, 188, 290, 329);
        getContentPane().add(treeDB);
        
        treeData.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(arg0.getClickCount() == 2) {
					TreePath tp = treeData.getPathForLocation(arg0.getX(), arg0.getY());					
					int depth = tp.getPathCount();
					
					if(depth == 1) {
						int returnVal = fc.showOpenDialog(ExplorerWin.this);        	
			        	if (returnVal == JFileChooser.APPROVE_OPTION) {
			                File file = fc.getSelectedFile();
			                //This is where a real application would open the file.
			                System.out.println("Opening: " + file.getName());			                			                
			                ctx.getDataProcessor().loadStatsData(ctx.getMode());
			        		ctx.getMDI().refreshExplorer();
			            } else {
			            	System.out.println("Open command cancelled by user.");
			            }
					}
					
					if(depth == 2) {
						Object[] pathNode = tp.getPath();
						String fieldName = pathNode[1].toString().trim();
						//if(pathNode[1].toString().trim().equalsIgnoreCase("TPS")) {
						//	ctx.getDataProcessor().
						//}
						try {
							//ChangeDataAttrWin changeDataAttrWin = new ChangeDataAttrWin(ctx.getDataProcessor().getFieldIdByName(fieldName), ctx);
							ctx.getMDI().showChangeAttrWin(ctx.getDataProcessor().getFieldIdByName(fieldName), ctx);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
					
					//System.out.println("path count:"  + tp.getPathCount());
					//System.out.println("parent:" + tp.getParentPath().toString());
					
					//System.out.println(pathNode[0].toString() + pathNode[1].toString() + pathNode[2].toString());
				}
			}
		});
        
        treeData.setModel(new DefaultTreeModel(
    			new DefaultMutableTreeNode("Data") {
    				{
    					for(int i=0; i<ctx.getDataProcessor().getStatsData().size(); i++) {
    						DefaultMutableTreeNode statsDataNode = new DefaultMutableTreeNode(ctx.getDataProcessor().getStatsData().get(i).getFieldName());
    						statsDataNodes.add(statsDataNode);
    						add(statsDataNode);
    						
    						/*
    						DefaultMutableTreeNode statsDataHighThdNode;
    						DefaultMutableTreeNode statsDataLowThdNode;
    						if(ctx.getDataProcessor().getStatsData().get(i).getHighThd() == Long.MAX_VALUE) {
    							statsDataHighThdNode = new DefaultMutableTreeNode("High threshold: not set");
    						}
    						else {
    							statsDataHighThdNode = new DefaultMutableTreeNode("High threshold:" + ctx.getDataProcessor().getStatsData().get(i).getHighThd());
    						}
    						statsDataNode.add(statsDataHighThdNode);
    						
    						if(ctx.getDataProcessor().getStatsData().get(i).getLowThd() == Long.MIN_VALUE) {
    							statsDataLowThdNode = new DefaultMutableTreeNode("Low threshold: not set");
    						}
    						else {
    							statsDataLowThdNode = new DefaultMutableTreeNode("Low threshold:" + ctx.getDataProcessor().getStatsData().get(i).getLowThd());
    						}
    						statsDataNode.add(statsDataLowThdNode);
    						*/
    					}
    					
    					/*
    					add(new DefaultMutableTreeNode("Elapsed Time"));
    					add(new DefaultMutableTreeNode("Message Sent (Periodic)"));
    					add(new DefaultMutableTreeNode("Message Sent (Cumulative)"));
    					add(new DefaultMutableTreeNode("Message Received (Periodic)"));
    					add(new DefaultMutableTreeNode("Message Received (Cumulative)"));
    					add(new DefaultMutableTreeNode("TPS")); */
    				}
    			}
    		));
        
        treeDB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(arg0.getButton() == arg0.BUTTON3) {
					//treeData.setVisible(false);
					//mnNew.setVisible(true);
				} 
				if(arg0.getClickCount() == 2) {
					ctx.getMDI().showNewChartWin(ctx);
					
				}
			}
		});
        
        treeDB.setModel(new DefaultTreeModel(
    			new DefaultMutableTreeNode("Chart") {
    				{
    					//DefaultMutableTreeNode node_1;
    					node_1 = new DefaultMutableTreeNode("Chart Window");
    						node_1.add(new DefaultMutableTreeNode("Add Chart..."));
    					add(node_1);
    					//node_1 = new DefaultMutableTreeNode("Report");
    					//	node_1.add(new DefaultMutableTreeNode("Add Report..."));
    					//add(node_1);
    				}
    			}
    		));
    	
    }
}