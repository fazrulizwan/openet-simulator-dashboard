
public class StatsData {
	private String fieldId;
	private String fieldName;
	private String dataType;
	private String value;
	private String unitName;
	private String unitId;
	private boolean isTransmitted = true;
	private Long highThd = Long.MAX_VALUE;
	private Long lowThd = Long.MIN_VALUE;
	
	public void setFieldId(String val) { fieldId = val; }
	public void setFieldName(String val) { fieldName = val; }
	public void setDataType(String val) { dataType = val; }
	public void setValue(String val) { value = val; }
	public void setIsTransmitted(boolean val) { isTransmitted = val; }
	public void setUnitName(String val) { unitName = val; }
	public void setUnitId(String val) { unitId = val; }
	public void setHighThd(Long val) { highThd = val; }
	public void setLowThd(Long val) { lowThd = val; }

	public String getFieldId() { return fieldId; }
	public String getFieldName() { return fieldName; }
	public String getDataType() { return dataType; }
	public boolean getIsTransmitted() { return isTransmitted; }
	public String getUnitName() { return unitName; }
	public String getUnitId() { return unitId; }
	public Long getHighThd() { return highThd; }
	public Long getLowThd() { return lowThd; }
	
	public int getValue() {
		return Integer.parseInt(value);
	}
}
