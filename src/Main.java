import java.awt.Color;

import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DLtd;


public class Main {

	public static void main(String[] args) {	
		
		int dataMode = 1;	
		
		Context ctx = new Context();
		ctx.setMode(dataMode);
		
		ChartWindowManager winMan = new ChartWindowManager();				
		
		StatsDataProcessor dataProc = new StatsDataProcessor();
        //dataProc.loadStatsData(dataMode);  
        ctx.setDataProcessor(dataProc);
        
        if(dataMode == 1) {
			//RemoteConnector con = new RemoteConnector("localhost", 60211, dataProc, winMan, ctx);
			//ctx.setRemoteConnector(con);
        } else if(dataMode == 2) {
        	CSVReaderManager csvReader = new CSVReaderManager(ctx);
        	ctx.setCSVReader(csvReader);
        }
		
		//ControlWindow ctrlWin = new ControlWindow(ctx);
		//ctrlWin.setVisible(true);
		
		//ExplorerWindow explorer = new ExplorerWindow(ctx);		
		//winMan.setExplorerWindow(explorer);
		
		ctx.setWindowManager(winMan);	
		
		//Playback pb = new Playback(ctx);
		//ctx.setPlayBack(pb);
		MDI mdi = new MDI(ctx);
		ctx.setMDI(mdi);
		mdi.setVisible(true);
		//con.establish();		
		
		//ProjectLoader prLoad = new ProjectLoader(ctx);
		//prLoad.load();		
				
		
		
	}

}
