import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONValue;
import org.json.simple.JSONObject;


public class RemoteConnector {

	private String host;
	private int port;
	private Socket sock;
	OutputStream outStream;
	InputStream inMessage;
	StatsDataProcessor dataProc;
	ChartWindowManager winMan;
	Context ctx;
	byte[] receiveBuf = new byte[1024];
	
	public RemoteConnector(String host, int port, StatsDataProcessor dataProc, ChartWindowManager winMan, Context ctx) {
		this.host = host;
		this.port = port;
		this.dataProc = dataProc;
		this.winMan = winMan;
		this.ctx = ctx;
	}
	
	private JSONObject sendSyncMsg(String requestMsg) {
		byte[] data;
		int byteLen;				
		String inMsgStr;
		Object obj;
        JSONObject jsonObj;        
        int respCode;
        String opCode;
		JSONParser parser = new JSONParser();
				
		data = requestMsg.getBytes();
		try {			
			outStream = sock.getOutputStream();
			inMessage = sock.getInputStream();					
			outStream.write(data);			
			
			if((byteLen = inMessage.read(receiveBuf)) != -1)
            {
				inMsgStr = new String(receiveBuf);
				receiveBuf = new byte[1024];
                inMsgStr = inMsgStr.trim();
                obj = parser.parse(inMsgStr);
                jsonObj = (JSONObject) obj;   
                opCode = (String) jsonObj.get("opcode");
                respCode = Integer.parseInt((String) jsonObj.get("response_status"));
                if(opCode.equals("RUN_RESPONSE") && respCode == 0) {
                	RemoteConnectorReader reader = new RemoteConnectorReader(sock, dataProc, winMan);
        	        Thread rcrThread = new Thread(reader);
        	        rcrThread.start();        	        
                }
                return jsonObj;                
            }
			
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		} catch (ParseException e) {			
			e.printStackTrace();
		}		
		
		return null;
	}
	
	public int run() {
		String requestMsg = "";
		Map map = new LinkedHashMap();
		
		map.put("opcode", "RUN");		    
	    requestMsg = JSONValue.toJSONString(map);
	    JSONObject jsonObj = sendSyncMsg(requestMsg);
	    String opCode = (String) jsonObj.get("opcode");
	    int respCode = Integer.parseInt((String) jsonObj.get("response_status"));
	    String respMsg = (String) jsonObj.get("response_message");
	    if(opCode.equals("RUN_RESPONSE") && respCode == 0) {
		    ctx.setSimRunning(true);        
	        return 0;
        } else {
        	return -1;
        }
	}
	
	public int establish() {
		String requestMsg = "";
		Map map = new LinkedHashMap();
		try {
			sock = new Socket(host, port);			
		} catch (UnknownHostException e) {			
			e.printStackTrace();
			return -1;
		} catch (IOException e) {			
			e.printStackTrace();
			return -1;
		} 
		
		
		
		map.put("opcode", "ESTABLISH");
		map.put("protocol_version", "1.0");	    
	    requestMsg = JSONValue.toJSONString(map);
	    JSONObject jsonObj = sendSyncMsg(requestMsg);
	    String opCode = (String) jsonObj.get("opcode");
	    int respCode = Integer.parseInt((String) jsonObj.get("response_status"));
	    String respMsg = (String) jsonObj.get("response_message");
	    if(opCode.equals("ESTABLISH_RESPONSE") && respCode == 0) {
		    ctx.setSimServConnected(true);
	    	ctx.setSimulatorName((String) jsonObj.get("simulator_type"));
	    	ctx.setSimulatorVersion((String) jsonObj.get("simulator_version"));        
	        
	        return 0;
        } else {
        	return -1;
        }        
	}	
}
